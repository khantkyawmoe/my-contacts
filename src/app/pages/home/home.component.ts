import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/providers/services/contact.service';
declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  contact: any = {};
  contacts: any[] = [];
  dataList: any[] = [];
  page = 1;
  perPage = 3; // three records at per page
  isSearch = false;

  constructor(
    public contactService: ContactService,
  ) {
    this.getContacts();
  }

  ngOnInit(): void {
  }

  getContacts() {
    this.contacts = [];
    this.dataList = [];
    this.contactService.getContacts().subscribe(results => {
      this.contacts = results;
      this.dataList = results;
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  onSearch(keyword: string) {
    if (keyword.length > 0) {
      this.isSearch = true;
      this.contacts = this.dataList.filter(contact => (contact.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) || (contact.email.toLowerCase().indexOf(keyword.toLowerCase()) !== -1) || (contact.phone.toLowerCase().indexOf(keyword.toLowerCase()) !== -1));
    } else {
      this.isSearch = false;
      this.contacts = this.dataList;
    }
  }

  openDeleteModal(contact: any) {
    this.contact = contact;
    jQuery('#deleteModalBox').modal('show');
  }

  deleteMyContact() {
    this.contactService.deleteContact(this.contact.id).subscribe(result => {
      this.contactService.showAlertMessage('Successfully Deleted.');
      this.getContacts();
      this.contact = {};
      jQuery('#deleteModalBox').modal('hide');
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  call(phone) {
    window.location.href = 'tel:' + phone;
  }

  mail(mail) {
    window.location.href = 'mailto:' + mail;
  }

}
