import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from 'src/app/providers/services/contact.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {

  id: any;
  contactForm: FormGroup;
  contactSubmit = false;
  contacts: any[] = [];
  validEmail = true;
  validPhone = true;

  constructor(
    private contactService: ContactService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {

    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('[0-9]+')]],
      email: ['', [Validators.required, Validators.email]]
    });

    this.id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.getContacts();
  }

  getContacts() {
    this.contacts = [];
    this.contactService.getContacts().subscribe(results => {
      this.contacts = results;
      if (this.id && this.id > 0) {
        this.contactForm.patchValue(this.contacts.filter(data => data.id === this.id)[0]);
        this.contacts = this.contacts.filter(data => data.id !== this.id);
      }
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  get valid() {
    return this.contactForm.controls;
  }

  isValidEmail(mail: string) {
    if (mail.length > 0) {
      const data: any = this.contacts.filter(contact => contact.email.toLowerCase() === mail.toLowerCase());
      if (data && data.length > 0) {
        this.validEmail = false;
      } else {
        this.validEmail = true;
      }
    } else {
      this.validEmail = true;
    }
  }

  isValidPhone(phone: string) {
    if (phone.length > 0) {
      const data: any = this.contacts.filter(contact => contact.phone === phone);
      if (data && data.length > 0) {
        this.validPhone = false;
      } else {
        this.validPhone = true;
      }
    } else {
      this.validPhone = true;
    }
  }

  submitContactForm() {
    this.contactSubmit = true;
    if (this.contactForm.invalid) {
      return;
    } else {
      if (this.id && this.id > 0) {
        this.updateContact();
      } else {
        this.contactForm.value.id = Math.floor(Math.random() * 900000 + 1);
        this.createContact();
      }
    }
  }

  updateContact() {
    this.contactService.updateContact(this.id, this.contactForm.value).subscribe(result => {
      this.goHome();
      this.contactService.showAlertMessage('Successfully updated.');
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  createContact() {
    this.contactService.createContact(this.contactForm.value).subscribe(result => {
      this.goHome();
      this.contactService.showAlertMessage('Successfully Created.');
    }, error => {
      console.log(JSON.stringify(error));
    });
  }

  goHome() {
    this.contactSubmit = false;
    this.contactForm.reset();
    this.router.navigate(['/home']);
  }

}
