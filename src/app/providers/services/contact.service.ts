import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  host: string;
  message = '';

  constructor(
    private http: HttpClient,
  ) {
    this.host = environment.host;
  }

  getContact(id: number): Observable<any> {
    return this.http.get<any>(this.host + '/contacts/' + id);
  }

  getContacts(): Observable<any[]> {
    return this.http.get<any[]>(this.host + '/contacts?_sort=name&_order=asc');
  }

  createContact(data: any): Observable<any> {
    return this.http.post(this.host + '/contacts', data);
  }

  updateContact(id: number, data: any): Observable<any> {
    return this.http.put(this.host + `/contacts/${id}`, data);
  }

  deleteContact(id: number): Observable<any[]> {
    return this.http.delete<any>(this.host + `/contacts/${id}`);
  }

  showAlertMessage(msg) {
    this.message = msg;
    setTimeout(() => {
      this.message = '';
    }, 3000);
  }
}
